import os
import subprocess
import shutil
import datetime
import time
import webbrowser
from queue import Queue
from threading import Thread
from PySide6.QtWidgets import QApplication, QWidget, QVBoxLayout, QLabel, QPushButton, QFileDialog, QListWidget, QLineEdit, QHBoxLayout, QComboBox
from PySide6.QtCore import Qt
from PySide6.QtGui import QDragEnterEvent, QDropEvent

output_video_file = None
video_queue = Queue()
processing_thread = None

def copy_video_to_working_dir(video_file):
    """Copy the selected video file to the current working directory with a unique name."""
    current_dir = os.getcwd()
    base_name = os.path.splitext(os.path.basename(video_file))[0]
    new_filename = f"{base_name}_{datetime.datetime.now().strftime('%Y%m%d_%H%M%S')}{os.path.splitext(video_file)[1]}"
    destination = os.path.join(current_dir, new_filename)
    shutil.copy(video_file, destination)
    return destination

def convert_video_to_audio(video_file, output_ext="mp3"):
    """Convert a video file to an audio file."""
    filename, _ = os.path.splitext(video_file)
    output_audio = f"{filename}.{output_ext}"
    subprocess.run(["ffmpeg", "-y", "-i", video_file, output_audio], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    print("Video converted to audio.")
    return output_audio

def translate_audio(audio_file, model, language, task):
    """Transcribe and translate the audio file."""
    output_audio = f"{os.path.splitext(audio_file)[0]}_translated.mp3"
    subprocess.run(["whisper", audio_file, "--model", model, "--language", language, "--task", task])
    print("Audio translated.")

def generate_subtitles(input_video):
    """Generate subtitles for a video file."""
    global output_video_file
    start_time = time.time()
    
    audio_file = convert_video_to_audio(input_video)
    
    model = app_window.model_input.currentText()
    language = app_window.language_input.currentText()
    task = app_window.task_input.currentText()

    translate_audio(audio_file, model, language, task)
    
    base_name = os.path.splitext(os.path.basename(input_video))[0]
    subtitle = f"{base_name}.srt"
    
    current_datetime = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    prefix = app_window.prefix_input.text()
    if prefix:
        output_video_file = f"{prefix}_{base_name}_with_subtitles_{current_datetime}.mp4"
    else:
        output_video_file = f"{base_name}_with_subtitles_{current_datetime}.mp4"
    
    try:
        subprocess.run(["ffmpeg", "-i", input_video, "-vf", f"subtitles={subtitle}", output_video_file])
    except subprocess.CalledProcessError:
        print("Failed to use GPU acceleration. Using CPU instead.")
        subprocess.run(["ffmpeg", "-i", input_video, "-vf", f"subtitles={subtitle}", output_video_file])
    
    end_time = time.time()
    total_time = end_time - start_time
    print(f"Subtitles generated and added to the video. Total time: {total_time:.2f} seconds")
    
    app_window.open_folder_button.setEnabled(True)

    app_window.update_queue_list()
    process_next_video()

def process_next_video():
    """Process the next video in the queue if available."""
    if not video_queue.empty():
        next_video = video_queue.get()
        generate_subtitles(next_video)
    else:
        global processing_thread
        processing_thread = None

class AppWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Video Subtitle Generator")
        self.setAcceptDrops(True)
        
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        self.instructions = QLabel("Drag and Drop a video file here or use the button below to select a file.")
        self.instructions.setAlignment(Qt.AlignCenter)
        self.layout.addWidget(self.instructions)

        self.prefix_layout = QHBoxLayout()
        self.prefix_label = QLabel("Prefix for output files, keep blank for skip:")
        self.prefix_input = QLineEdit()
        self.prefix_layout.addWidget(self.prefix_label)
        self.prefix_layout.addWidget(self.prefix_input)
        self.layout.addLayout(self.prefix_layout)

        self.select_button = QPushButton("Select Video File")
        self.select_button.clicked.connect(self.select_file)
        self.layout.addWidget(self.select_button)

        self.model_layout = QHBoxLayout()
        self.model_label = QLabel("Whisper Model:")
        self.model_input = QComboBox()
        self.model_input.addItems(["tiny", "base", "small", "medium", "large"])
        self.model_input.setCurrentText("medium")
        self.model_layout.addWidget(self.model_label)
        self.model_layout.addWidget(self.model_input)
        self.layout.addLayout(self.model_layout)

        self.language_layout = QHBoxLayout()
        self.language_label = QLabel("Input video voice Language:")
        self.language_input = QComboBox()
        self.language_input.addItems([
    "en", "ru", "es", "fr", "de",
    "af", "am", "ar", "as", "az", "ba", "be", "bg", "bn", "bo", "br", "bs",
    "ca", "cs", "cy", "da", "el", "et", "eu", "fa", "fi", "fo", "gl", "gu",
    "ha", "haw", "he", "hi", "hr", "ht", "hu", "hy", "id", "is", "it", "ja",
    "jw", "ka", "kk", "km", "kn", "ko", "la", "lb", "ln", "lo", "lt", "lv",
    "mg", "mi", "mk", "ml", "mn", "mr", "ms", "mt", "my", "ne", "nl", "nn",
    "no", "oc", "pa", "pl", "ps", "pt", "ro", "sa", "sd", "si", "sk", "sl",
    "sn", "so", "sq", "sr", "su", "sv", "sw", "ta", "te", "tg", "th", "tk",
    "tl", "tr", "tt", "uk", "ur", "uz", "vi", "yi", "yo", "yue", "zh"
])

        self.language_input.setCurrentText("ru")
        self.language_layout.addWidget(self.language_label)
        self.language_layout.addWidget(self.language_input)
        self.layout.addLayout(self.language_layout)

        self.task_layout = QHBoxLayout()
        self.task_label = QLabel("Task: Add to output video ")
        self.task_input = QComboBox()
        self.task_input.addItems(["transcribe", "translate"])
        self.task_input.setCurrentText("translate")
        self.task_layout.addWidget(self.task_label)
        self.task_layout.addWidget(self.task_input)
        self.layout.addLayout(self.task_layout)

        self.start_button = QPushButton("Start Processing")
        self.start_button.clicked.connect(self.start_processing)
        self.layout.addWidget(self.start_button)

        self.open_folder_button = QPushButton("Open Output Folder")
        self.open_folder_button.setEnabled(False)
        self.open_folder_button.clicked.connect(self.open_output_folder)
        self.layout.addWidget(self.open_folder_button)

        self.queue_list = QListWidget()
        self.layout.addWidget(self.queue_list)

    def dragEnterEvent(self, event: QDragEnterEvent):
        if event.mimeData().hasUrls():
            event.acceptProposedAction()

    def dropEvent(self, event: QDropEvent):
        for url in event.mimeData().urls():
            file_path = url.toLocalFile()
            if file_path:
                video_file = copy_video_to_working_dir(file_path)
                video_queue.put(video_file)
                self.update_queue_list()

    def select_file(self):
        file_path, _ = QFileDialog.getOpenFileName(self, "Select a video file", "", "Video files (*.mp4 *.avi *.mkv)")
        if file_path:
            video_file = copy_video_to_working_dir(file_path)
            video_queue.put(video_file)
            self.update_queue_list()

    def start_processing(self):
        global processing_thread
        if processing_thread is None:
            processing_thread = Thread(target=process_next_video)
            processing_thread.start()

    def open_output_folder(self):
        output_folder = os.getcwd()
        webbrowser.open(output_folder)

    def update_queue_list(self):
        """Update the GUI list widget to reflect the current queue."""
        self.queue_list.clear()
        for video_file in list(video_queue.queue):
            self.queue_list.addItem(os.path.basename(video_file))

app = QApplication([])

app_window = AppWindow()
app_window.show()

app.exec()
