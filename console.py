import os
import subprocess
import datetime
import time
# if you see issue like cpu is using not cuda GPU
#pip3 uninstall -y torch torchvision torchaudio
# following command was generated using https://pytorch.org/get-started/locally/#with-cuda-1
#pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118 

def convert_video_to_audio(video_file, output_ext="mp3"):
    """Convert a video file to an audio file."""
    filename, _ = os.path.splitext(video_file)
    output_audio = f"{filename}.{output_ext}"
    subprocess.run(["ffmpeg", "-y", "-i", video_file, output_audio], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    print("Video converted to audio.")
    return output_audio

def translate_audio(audio_file, output_language="en"):
    """Transcribe and translate the audio file."""
    output_audio = f"{os.path.splitext(audio_file)[0]}_translated.mp3"
    subprocess.run(["whisper", audio_file, "--model", "medium", "--language", output_language, "--task", "translate"])
    print("Audio translated.")

def generate_subtitles(input_video):
    """Generate subtitles for a video file."""
    start_time = time.time()
    
    # Convert video to audio
    audio_file = convert_video_to_audio(input_video)
    
    # Translate audio
    translate_audio(audio_file)
    
    # Generate subtitle filename
    base_name = os.path.splitext(os.path.basename(input_video))[0]
    subtitle = f"{base_name}.srt"
    
    # Modify output video filename
    current_datetime = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    output_video = f"{base_name}_with_subtitles_{current_datetime}.mp4"
    
    # Attempt GPU acceleration with NVENC, fallback to CPU if fails
    try:
        subprocess.run(["ffmpeg", "-i", input_video, "-vf", f"subtitles={subtitle}", "-c:v", "h264_nvenc", output_video])
    except subprocess.CalledProcessError:
        print("Failed to use GPU acceleration. Using CPU instead.")
        subprocess.run(["ffmpeg", "-i", input_video, "-vf", f"subtitles={subtitle}", output_video])
    end_time = time.time()
    total_time = end_time - start_time
    print(f"Subtitles generated and added to the video. Total time: {total_time:.2f} seconds")

if __name__ == "__main__":
    input_video = 'a.mp4'
    generate_subtitles(input_video)
