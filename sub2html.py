import os

def convert_txt_to_html(input_path, output_path):
    """
    Converts a .txt file to an .html file with basic styling.
    """
    with open(input_path, 'r', encoding='utf-8') as file:
        content = file.read()
    
    html_content = f"""
    <html>
    <head>
        <title>{os.path.basename(input_path)}</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        <div class="theme-switcher">
            <label for="theme">Choose a theme:</label>
            <select id="theme">
                <option value="light">Light</option>
                <option value="dark">Dark</option>
                <option value="dracula">Dracula</option>
                <option value="peachpuff">Peachpuff</option>
            </select>
        </div>
        <pre>{content}</pre>
        <script src="script.js"></script>
    </body>
    </html>
    """
    
    with open(output_path, 'w', encoding='utf-8') as file:
        file.write(html_content)

def generate_index(root_dir, index_path):
    """
    Generates an index.html file that links to all the .html files.
    """
    index_content = f"""
    <html>
    <head>
        <title>Index of {root_dir}</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        <h1>Index of {root_dir}</h1>
        <div class="theme-switcher">
            <label for="theme">Choose a theme:</label>
            <select id="theme">
                <option value="light">Light</option>
                <option value="dark">Dark</option>
                <option value="dracula">Dracula</option>
                <option value="peachpuff">Peachpuff</option>
            </select>
        </div>
        <ul>
    """
    
    for root, dirs, files in os.walk(root_dir):
        relative_path = os.path.relpath(root, root_dir)
        for file in files:
            if file.endswith('.html'):
                file_path = os.path.join(relative_path, file)
                index_content += f'<li><a href="{file_path}">{file_path}</a></li>\n'
    
    index_content += """
        </ul>
        <script src="script.js"></script>
    </body>
    </html>
    """
    
    with open(index_path, 'w', encoding='utf-8') as file:
        file.write(index_content)

def main(root_dir):
    """
    Main function to convert all .txt files to .html and generate an index.
    """
    for root, dirs, files in os.walk(root_dir):
        for file in files:
            if file.endswith('.txt'):
                input_path = os.path.join(root, file)
                output_path = os.path.splitext(input_path)[0] + '.html'
                convert_txt_to_html(input_path, output_path)
    
    generate_index(root_dir, os.path.join(root_dir, 'index.html'))

    # Create a CSS file with themes
    with open(os.path.join(root_dir, 'style.css'), 'w', encoding='utf-8') as file:
        file.write("""
        :root {
            --background-color: white;
            --text-color: black;
            --link-color: blue;
            --link-hover-color: darkblue;
            --padding: 20px;
            --margin: 20px;
            --border-radius: 10px;
            --box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }
        body {
            font-family: 'Arial', sans-serif;
            background-color: var(--background-color);
            color: var(--text-color);
            margin: var(--margin);
        }
        pre {
            white-space: pre-wrap;
            word-wrap: break-word;
            background-color: var(--background-color);
            color: var(--text-color);
            padding: var(--padding);
            border-radius: var(--border-radius);
            box-shadow: var(--box-shadow);
            font-size: 1rem;
            line-height: 1.5;
        }
        a {
            text-decoration: none;
            color: var(--link-color);
        }
        a:hover {
            text-decoration: underline;
            color: var(--link-hover-color);
        }
        .theme-switcher {
            margin-bottom: var(--margin);
        }
        .theme-switcher label {
            margin-right: 10px;
            font-size: 1.2rem;
        }
        select#theme {
            padding: 5px;
            font-size: 1rem;
            border-radius: var(--border-radius);
            box-shadow: var(--box-shadow);
        }
        h1 {
            font-size: 2rem;
            margin-bottom: var(--margin);
        }
        ul {
            list-style-type: none;
            padding: 0;
        }
        ul li {
            margin-bottom: 10px;
            font-size: 1.2rem;
        }
        /* Light Theme */
        body.light {
            --background-color: white;
            --text-color: black;
            --link-color: blue;
            --link-hover-color: darkblue;
        }
        /* Dark Theme */
        body.dark {
            --background-color: #121212;
            --text-color: #ffffff;
            --link-color: #bb86fc;
            --link-hover-color: #3700b3;
        }
        /* Dracula Theme */
        body.dracula {
            --background-color: #282a36;
            --text-color: #f8f8f2;
            --link-color: #bd93f9;
            --link-hover-color: #ff79c6;
        }
        /* Peachpuff Theme */
        body.peachpuff {
            --background-color: peachpuff;
            --text-color: #333333;
            --link-color: #ff4500;
            --link-hover-color: #d2691e;
        }
        """)

    # Create a JavaScript file for theme switching
    with open(os.path.join(root_dir, 'script.js'), 'w', encoding='utf-8') as file:
        file.write("""
        document.addEventListener('DOMContentLoaded', () => {
            const themeSwitcher = document.getElementById('theme');
            themeSwitcher.addEventListener('change', (event) => {
                document.body.className = event.target.value;
            });
            // Set the initial theme based on previous selection if available
            const savedTheme = localStorage.getItem('theme');
            if (savedTheme) {
                document.body.className = savedTheme;
                themeSwitcher.value = savedTheme;
            }
            themeSwitcher.addEventListener('change', (event) => {
                const selectedTheme = event.target.value;
                document.body.className = selectedTheme;
                localStorage.setItem('theme', selectedTheme);
            });
        });
        """)

if __name__ == "__main__":
    root_directory = os.getcwd()
    main(root_directory)
