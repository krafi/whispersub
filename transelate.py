import ollama
import os
import tkinter as tk
from tkinter import filedialog, messagebox
from tkinterdnd2 import DND_FILES, TkinterDnD
import queue
import threading

MODEL = 'llama3'

def chat_with_model(user_message):
    response = ollama.chat(model=MODEL, messages=[{'role': 'user', 'content': user_message}])
    return response['message']['content']

def translate_text_chunk(chunk):
    prompt = ("Translate the following English sentences to Russian. "
              "Only provide the original text (English) and translations (Russian): "
              "and nothing else because you are unsocial and only do your work:\n")
    for line in chunk:
        prompt += f"English: {line}\nRussian: "
    response = chat_with_model(prompt)
    return response

def translate_file(file_path, chunk_size):
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            lines = file.readlines()

        base_name = os.path.basename(file_path).rsplit('.', 1)[0]
        output_file = f"{base_name}_translated.txt"
        output_path = os.path.join(os.getcwd(), output_file)

        with open(output_path, 'w', encoding='utf-8') as out_file:
            for i in range(0, len(lines), chunk_size):
                chunk = lines[i:i+chunk_size]
                chunk = [line.strip() for line in chunk if line.strip()]
                if chunk:
                    translation = translate_text_chunk(chunk)
                    out_file.write(translation + '\n')

        return output_path
    except FileNotFoundError:
        return None
    except Exception as e:
        print(f"Error: {e}")
        return None

class TranslatorApp(TkinterDnD.Tk):
    def __init__(self):
        super().__init__()

        self.title("Llama3 Translator")
        self.geometry("400x500")

        self.file_path = tk.StringVar()
        self.chunk_size = tk.IntVar(value=5)
        self.queue = queue.Queue()
        self.processing = False

        self.create_widgets()

    def create_widgets(self):
        self.file_label = tk.Label(self, text="Select a file to translate:")
        self.file_label.pack(pady=5)

        self.file_entry = tk.Entry(self, textvariable=self.file_path, width=40)
        self.file_entry.pack(pady=5)

        self.browse_button = tk.Button(self, text="Browse", command=self.browse_file)
        self.browse_button.pack(pady=5)

        self.chunk_label = tk.Label(self, text="Enter chunk size:")
        self.chunk_label.pack(pady=5)

        self.chunk_entry = tk.Entry(self, textvariable=self.chunk_size)
        self.chunk_entry.pack(pady=5)

        self.translate_button = tk.Button(self, text="Add to Queue", command=self.add_to_queue)
        self.translate_button.pack(pady=5)

        self.queue_label = tk.Label(self, text="Queued Tasks:")
        self.queue_label.pack(pady=5)

        self.queue_listbox = tk.Listbox(self, width=50, height=10)
        self.queue_listbox.pack(pady=5)

        self.start_button = tk.Button(self, text="Start", command=self.start_processing)
        self.start_button.pack(pady=5)

        self.drop_target_register(DND_FILES)
        self.dnd_bind('<<Drop>>', self.on_drop)

    def browse_file(self):
        file_path = filedialog.askopenfilename()
        if file_path:
            self.file_path.set(file_path)

    def on_drop(self, event):
        file_path = event.data
        if file_path.startswith('{') and file_path.endswith('}'):
            file_path = file_path[1:-1]
        self.file_path.set(file_path)
        self.add_to_queue()

    def add_to_queue(self):
        file_path = self.file_path.get()
        chunk_size = self.chunk_size.get()

        if not file_path:
            messagebox.showerror("Error", "Please select a file to translate.")
            return

        if not isinstance(chunk_size, int) or chunk_size <= 0:
            messagebox.showerror("Error", "Please enter a valid chunk size.")
            return

        task = (file_path, chunk_size)
        self.queue.put(task)
        self.update_queue_listbox()
        self.file_path.set("")

    def update_queue_listbox(self):
        self.queue_listbox.delete(0, tk.END)
        for i, item in enumerate(list(self.queue.queue)):
            self.queue_listbox.insert(tk.END, f"Task {i+1}: {os.path.basename(item[0])}, Chunk Size: {item[1]}")

    def start_processing(self):
        if not self.processing:
            self.processing = True
            self.translation_thread = threading.Thread(target=self.process_queue)
            self.translation_thread.setDaemon(True)
            self.translation_thread.start()
            messagebox.showinfo("Info", "Started processing the queue.")

    def process_queue(self):
        while not self.queue.empty():
            file_path, chunk_size = self.queue.get()
            self.translate_worker(file_path, chunk_size)
            self.queue.task_done()
            self.update_queue_listbox()
        self.processing = False
        messagebox.showinfo("Success", "All tasks have been processed successfully!")

    def translate_worker(self, file_path, chunk_size):
        translate_file(file_path, chunk_size)

if __name__ == "__main__":
    app = TranslatorApp()
    app.mainloop()
