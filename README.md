this project is inspaird by https://colab.research.google.com/github/amrrs/subtitle-embedded-video-generator/blob/main/Generate_English_Subtitles_Video_with_OpenAI_Whisper.ipynb#scrollTo=EeOhGIRBnU5O project only the issue is you have to modify to ! pip install openai-whisper==20230117 . because lasterst whisper dont support

<!-- This is a comment in Markdown 

### Google Colab
[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1MAIJGgnrTyT1jPCbihCkbAGLjOcQmhBv#scrollTo=NquZq-CvbU-n) still working on it beta
-->
must thing to do:
dependencies:
1. install choco on your windows
copy peaste on your windows terminal

```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

```

now install ffmpeg-full,  
```powershell
choco install ffmpeg-full
```

method 1: use exe
here is the exe,
https://drive.google.com/file/d/1qX3lkjEAQgV80E9m6iQwM5KYmJ0bYWih/view?usp=sharing

method 2: use python

i used python 9.9.9
pip install webbrowser PySide6

For a more user-friendly alternative, I recommend trying our project at https://github.com/dsymbol/decipher. It offers similar functionality with an easier interface.


## Features

### Drag-and-Drop Interface
Users can drag and drop video files directly into the application for processing, or select files manually.

### Video to Audio Conversion
- **Transcription and Translation**: Uses Whisper for transcription and translation.

### Subtitle Embedding
- Embeds generated subtitles into the video using FFmpeg, creating a new video file with subtitles.

### Processing Queue
- Manages a queue of video files to be processed.
- Displays the queue in the GUI.

### User-Friendly Interface
- Provides an intuitive and easy-to-use interface for users.


